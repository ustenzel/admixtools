#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "nicksrc/nicklib.h"
#include "nicksrc/getpars.h"
#include "globals.h"

#include "badpairs.h"
#include "admutils.h"
#include "mcio.h"
#include "mcmcpars.h"
#include "regsubs.h"
#include "egsubs.h"
#include "qpsubs.h"
#include "stream.h"

/** 
 incorporates a jackknife to estimate branch lengths of a tree in f_st 
 space together with standard errors.  This is a test for whether a simple 
 phylogenetic split is supported by the data 
*/


#define WVERSION   "701"
// clade hits and misses (migrations?)
// forcclade added
// outpop NONE forced 
// print number of samples / pop
// popfilename added 
// bbestmode  
// No abc 
// f4mode = YES (no normalization) 
// printsd
// nochrom added
// BAD bug fix (sign of dstat)
// repeated pops trapped in popfilename quads
// id2pops added
// nzdata;  require 10 blocks with abba/baba counts
// xmode ;  call countpopsx which deals with gender on X
// syntactic sugar (strip :) in popfilename  
// numchrm added

#define MAXFL  50
#define MAXSTR  512
#define MAXPOPS 100

char *parname = NULL;
char *trashdir = "/var/tmp";
int colcalc = YES;
// int fstdetails = NO ;
int qtmode = NO;
int hires = NO;
int printsd = NO;

Indiv **indivmarkers;
SNP **snpmarkers;
int numsnps, numindivs;
int isinit = NO;
int markerscore = NO;
int seed = 0;
int chisqmode = NO;             // approx p-value better to use F-stat
int missingmode = NO;
int dotpopsmode = YES;
int noxdata = YES;              /* default as pop structure dubious if Males and females */
int pcorrmode = NO;
int pcpopsonly = YES;
int nostatslim = 10;
int znval = -1;
int popsizelimit = -1;
int gfromp = NO;                // genetic distance from physical 
int msjack = NO;
char *msjackname = NULL;
int msjackweight = YES;         // weighted jackknife
int bankermode = NO;
int forceclade = NO;
int bbestmode = YES;
int numbanker = 0;
int xchrom = -1;
int zchrom = -1;
int f4mode = NO;
int xmode = NO;
int printssize = YES;
int locount = -1, hicount = 9999999;
// if bankermode  bankers MUST be in quartet ;  at least one type 1 in quartet

int jackweight = YES;
double jackquart = -1.0;

double plo = .001;
double phi = .999;
double pvhit = .001;
double pvjack = 1.0e-6;
double blgsize = 0.05;          // block size in Morgans */
double *chitot;
int *xpopsize;

char *genotypename = NULL;
char *snpname = NULL;
char *snpoutfilename = NULL;
char *indivname = NULL;
char *badsnpname = NULL;
char *poplistname = NULL;
char *popfilename = NULL;
char *outliername = NULL;
char *outpop = NULL;
// list of outliers
int fullmode = NO;
int inbreed = NO;
double lambdascale;
int *f2ind, *ind2f;

int dotree = NO;

char *outputname = NULL;
char *weightname = NULL;
char *id2pops = NULL;
FILE *ofile;

void readcommands (int argc, char **argv);
double getrs (double *f4, double *f4sig, double *f2, double *f2sig,
              int a, int b, int c, int d, int numeg, double *rho);
int islegal (int *xind, int *types, int mode, int n);
int isclade (int *rr, int *zz);
void regesttree (double *ans, double *xn, double xd);
void ckdups (char **list, int n);
void printbadql (char ***qlist, int tt, char **eglist, int numeg);

struct dsc_state {
    double *ztop, *zbot, *znum;
    int *nzd;
    double zbaba, zabba ;
    int ntot ;
} ;

static inline void getdsc_pre(struct dsc_state *self, int nblocks)
{
    ZALLOC (self->nzd, nblocks, int);
    ZALLOC (self->ztop, nblocks, double);
    ZALLOC (self->zbot, nblocks, double);
    ZALLOC (self->znum, nblocks, double);
    self->ntot = 0;
    self->zbaba = 0;
    self->zabba = 0;
}

static inline void
getdsc_step (struct dsc_state *self,  double *abx, double *bax,
        int a, int b, int c, int d, int numeg, int bcol)
{
        int h1 = f2ind[a * numeg + b];
        int h2 = f2ind[c * numeg + d];
        int s = bcol;
        if (s < 0)
            return;
        double y1 = abx[h1];
        double y2 = bax[h2];

        if (y1 < 0.0)
            return;
        if (y2 < 0.0)
            return;

        double yabba = y1 * y2;

        y1 = bax[h1];
        y2 = abx[h2];

        if (y1 < 0.0)
            return;
        if (y2 < 0.0)
            return;

        yabba += y1 * y2;

        y1 = abx[h1];
        y2 = abx[h2];

        if (y1 < 0.0)
            return;
        if (y2 < 0.0)
            return;

        double ybaba = y1 * y2;

        y1 = bax[h1];
        y2 = bax[h2];

        if (y1 < 0.0)
            return;
        if (y2 < 0.0)
            return;

        ybaba += y1 * y2;

        self->zbaba += ybaba;
        self->zabba += yabba;

        double sgn = 1.0;
        if (a > b)
            sgn *= -1.0;
        if (c > d)
            sgn *= -1.0;

        self->ztop[s] += (ybaba - yabba) * sgn;
        if (f4mode == NO)
            self->zbot[s] += ybaba + yabba;
        if (f4mode == YES)
            self->zbot[s] += 1.0;

        self->znum[s] += 1.0;
        self->ntot++;

        y1 = fabs (yabba + ybaba);
        if (y1 > 0.0)
            self->nzd[s] = 1;

        /**
          if ((y1>0.0)) {
          printf("zznzd d %d %9.3f %9.3f %9.3f %9.3f %d %d\n", s, ybaba, yabba, ztop[s], zbot[s]) ; 
          }
          */
}

static inline void getdsc_post(struct dsc_state *self, double *dzscx, double *dzsc,
                 int a, int b, int c, int d, int nblocks, int *sz)
{
    // nzdata is # blocks with  nonzero abba or baba counts 
    int nzdata = intsum (self->nzd, nblocks);
    free (self->nzd);
    if (verbose)
        printf ("nzdata:  %d\n", nzdata);
    if (nzdata < 5) {
        free (self->ztop);
        free (self->zbot);
        free (self->znum);
        *dzscx = 0;                 // Z score
        *dzsc = 0;
        return;
    }
    double *jmean, *jwt;
    ZALLOC (jmean, nblocks, double);
    ZALLOC (jwt, nblocks, double);

    double ytop = asum (self->ztop, nblocks);
    double ybot = asum (self->zbot, nblocks);
    ybot += 1.0e-20;
    double ymean = ytop / ybot;
    for (int s = 0; s < nblocks; ++s) {
        double yt = ytop - self->ztop[s];
        double yb = ybot - self->zbot[s];
        jmean[s] = yt / yb;
        jwt[s] = self->znum[s];
    }
    // printf("## zzmean: %d %12.6f\n", nblocks, ymean) ; 

    /**
      for (s=0; s<nblocks; ++s) { 
      y1 = ztop[s] ; 
      y2 = zbot[s] ; 
      printf("zzbug: %d %9.6f %9.6f %9.6f\n", s, y1, y2, y1/(y2+1.0e-20)) ; 
      }
      */
    // abort() ;
    double est, sig;
    weightjack (&est, &sig, ymean, jmean, jwt, nblocks);
    *dzscx = est / (sig + 1.0e-20);       // Z score
    *dzsc = est;

    free (self->ztop);
    free (self->zbot);
    free (self->znum);
    free (jmean);
    free (jwt);

    if (sz == NULL)
        return;
    else
        ivzero (sz, 3);

    double sgn = 1.0;
    if (a > b)
        sgn *= -1.0;
    if (c > d)
        sgn *= -1.0;

    if (sgn < 0) {
        double yt = self->zbaba;
        self->zbaba = self->zabba;
        self->zabba = yt;
    }
    sz[0] = nnint (self->zbaba);
    sz[1] = nnint (self->zabba);
    sz[2] = self->ntot;


    //   printf("zz %12.6f ",  est) ;  
    //   printimat(sz, 1, 3) ;
}


#define NPAR 5
#define ND   6
struct gettreelen_state {
    double gden ;
    double *gn, **xn, *xden;
    double *wjack;
} ;

void gettreelen_pre (struct gettreelen_state *self, int nblocks)
{
    self->gden = 0 ;
    ZALLOC (self->gn, ND, double);
    ZALLOC (self->xden, nblocks, double);
    ZALLOC (self->wjack, nblocks, double);
    self->xn = initarray_2Ddouble (nblocks, ND, 0.0);
}

void gettreelen_step (struct gettreelen_state *self, double *f2,
                      double *abx, double *bax, int *ttt, int numeg, int s)
{
    int a = ttt[0];
    int b = ttt[1];
    int c = ttt[2];
    int d = ttt[3];

    int hh[6], i;
    int h1 = hh[0] = f2ind[a * numeg + b];
    hh[1] = f2ind[a * numeg + c];
    hh[2] = f2ind[a * numeg + d];
    hh[3] = f2ind[b * numeg + c];
    hh[4] = f2ind[b * numeg + d];
    int h2 = hh[5] = f2ind[c * numeg + d];

    if (s < 0) return;

    double y1 = abx[h1];
    double y2 = bax[h2];

    if (y1 < 0.0)
        return;
    if (y2 < 0.0)
        return;

    double yabba = y1 * y2;
    y1 = bax[h1];
    y2 = abx[h2];

    if (y1 < 0.0)
        return;
    if (y2 < 0.0)
        return;

    yabba += y1 * y2;
    y1 = abx[h1];
    y2 = abx[h2];
    double ybaba = y1 * y2;
    y1 = bax[h1];
    y2 = bax[h2];
    ybaba += y1 * y2;
    double yden = yabba + ybaba;
    self->xden[s] += yden;
    self->gden += yden;
    self->wjack[s] += 1.0;

    for (i = 0; i < ND; i++) {
        int h = hh[i];
        double y = f2[h];
        self->xn[s][h] += y;
        self->gn[h] += y;
    }
}

void
gettreelen_post (struct gettreelen_state *self, double *tlenz, double *tlen, int nblocks)
{
    double *qqest;
    ZALLOC (qqest, NPAR, double);
    double **xqest = initarray_2Ddouble (nblocks, NPAR, 0.0);

    double *djack;
    ZALLOC (djack, nblocks, double);

    regesttree (qqest, self->gn, self->gden);
    copyarr (qqest, tlen, NPAR);
    printf ("qqest: ");
    printmatw (qqest, 1, 5, 5);
    verbose = NO;

    for (int s = 0; s < nblocks; s++) {
        double *xx = self->xn[s];
        vvm (xx, self->gn, xx, ND);
        regesttree (xqest[s], xx, self->gden - self->xden[s]);
    }
    for (int a = 0; a < NPAR; a++) {
        for (int k = 0; k < nblocks; ++k) {
            djack[k] = xqest[k][a];
        }
        double jest, jsig;
        weightjack (&jest, &jsig, qqest[a], djack, self->wjack, nblocks);
        tlenz[a] = jest / jsig;
        tlen[a] = jest;
    }

    free2D (&xqest, nblocks);
    free2D (&self->xn, nblocks);
    free (self->xden);
    free (self->wjack);
    free (self->gn);
    free (djack);
    free (qqest);
}

static inline void setabx (double *abx, double *bax, int **ccc, int numeg)
{
    for (int i = 0; i < numeg; i++) {
        for (int j = i + 1; j < numeg; j++) {

            int t1 = intsum (ccc[i], 2);
            int t2 = intsum (ccc[j], 2);
            int h = f2ind[i * numeg + j];

            if( t1 != 0 && t2 != 0 ) {
                double y1 = (double) ccc[i][0] / (double) t1;
                double y2 = (double) ccc[j][0] / (double) t2;

                abx[h] = y1 * (1 - y2);
                bax[h] = y2 * (1 - y1);
            }
            else {
                abx[h] = -1.0;
                bax[h] = -1.0;
            }
        }
    }
}

inline static void setf2 (double *f2, int **ccc,  int numeg)
{
    for (int i = 0; i < numeg; i++) {
        for (int j = i + 1; j < numeg; j++) {

            double t1 = intsum (ccc[i], 2);
            double t2 = intsum (ccc[j], 2);
            if (t1 == 0)
                continue;
            if (t2 == 0)
                continue;

            double y1 = (double) ccc[i][0] / t1;
            double y2 = (double) ccc[j][0] / t2;

            double yy = (y1 - y2) * (y1 - y2);
            double x0 = ccc[i][0];
            double x1 = ccc[i][1];
            double h1 = x0 * x1 / (t1 * (t1 - 1));
            yy -= h1 / t1;

            x0 = ccc[j][0];
            x1 = ccc[j][1];
            double h2 = x0 * x1 / (t2 * (t2 - 1));
            yy -= h2 / t2;

            int h = f2ind[i * numeg + j];
            f2[h] = yy;
        }
    }
}


int
main (int argc, char **argv)
{

  char sss[MAXSTR];
  int **snppos;
  int *snpindx;
  char **snpnamelist, **indnamelist;
  char **eglist;
  int *nsamppops;
  int *ztypes;
  int lsnplist, lindlist, numeg;
  int i, j, k, k1, k2, k3, k4, kk;
  SNP *cupt, *cupt1, *cupt2, *cupt3;
  Indiv *indx;
  double y1, y2, y, sig, tail, yy1, yy2;
  char ss[11];
  int *blstart, *blsize, nblocks;
  int xnblocks;                 /* for xsnplist */
  double maxgendis;
  int xind[4];

  int ch1, ch2;
  int fmnum, lmnum;
  int num, n1, n2;

  int e, f, lag = 1;
  double xc[9], xd[4], xc2[9];
  int nignore, numrisks = 1;
  double *xrow, *xpt;
  // SNP **xsnplist;
  int *tagnums;
  Indiv **xindlist;
  int *xindex, *xtypes;
  int nrows, m, nc;
  double zn, zvar;
  int weightmode = NO;
  double chisq, ynrows;
  int *numhits, t;
  double *xmean, *xfancy;
  double *divans, *divsd;
  double *hettop, *hetbot;
  int chrom, numclear;
  double gdis;
  int outliter, *badlist, nbad;
  double **zdata, *z1, *z2;
  int maxtag = -1;
  double **zz, *pmean, *pnum;
  double rscore[3], dstat[3], hscore[3], rrr[3], ww[3], serr[3];
  int ssize[3][3], *sz;
  int tpat[3][4], rpat[3][4], *rrtmp, *rp;
  double *qpscores;
  double *hest, *hsig;
  double mingenpos, maxgenpos;
  int *qhit;                    /* number of times pair is clade in quartet */
  int *qmiss;                   /* number of times pair migration event implied */
  int **qplist, numqp = 0, maxqp = 10000;
  double *qpscore;
  char ***qlist, *sx;
  int nqlist = 0;
  int bbest[3];
  double absscore[3];
  double ascore[4], astat[4];

  double **dsctop, **dscbot;
  int popx[4];
  double tn[4 * 5], td[4 * 4];
  double zzsig[5], zzest[5], zsc[5];
  double ymin;

  double *f3, *f4, *f3sig, *f4sig;
  int t1, t2, tt;

  double tlenz[5], tlen[5];
  int lenz[5];


  readcommands (argc, argv);
  printf ("## qpDstat version: %s\n", WVERSION);
  if (parname == NULL)
    return 0;
  if ((poplistname == NULL) && (popfilename == NULL))
    fatalx ("poplistname, popfilename both null\n");

  if (!bankermode)
    forceclade = NO;
//if (fancynorm) printf("fancynorm used\n") ;
//else printf("no fancynorm used\n") ;
  setjquart (NO, jackweight, jackquart);

  nostatslim = MAX (nostatslim, 3);

  setinbreed (inbreed);

  if (outputname != NULL)
    openit (outputname, &ofile, "w");

  fprintf (stderr,"reading SNPs\n");
  numsnps =
    getsnps (snpname, &snpmarkers, 0.0, badsnpname, &nignore, numrisks);

  fprintf (stderr,"%d SNPs\n", numsnps);
  fprintf (stderr,"reading inDUHviduals\n");
  numindivs = getindivs (indivname, &indivmarkers);
  if (id2pops != NULL) {
    setid2pops (id2pops, indivmarkers, numindivs);
  }
  fprintf (stderr,"%d inDUHviduals\n",numindivs);
  setindm (indivmarkers);

  struct geno_stream genos ;
  if( -1 == open_genostream( &genos, genotypename ) ) {
    fprintf( stderr, "couldn't gzopen() genotype file %s\n", genotypename ) ;
    exit(1) ;
  }

  for (i = 0; i < numsnps; i++) {
    cupt = snpmarkers[i];
    if (cupt->chrom >= (numchrom + 1))
      cupt->ignore = YES;
    if (cupt->chrom == zchrom)
      cupt->ignore = YES;
  }

  ZALLOC (eglist, numindivs, char *);
  ZALLOC (ztypes, numindivs, int);
  if (popfilename == NULL) {
    if (bankermode == NO)
      numeg = loadlist (eglist, poplistname);
    else {
      numeg = loadlist_type (eglist, poplistname, ztypes, 0);
      numbanker = 0;
      for (k = 0; k < numeg; ++k) {
        if (ztypes[k] == 2)
          ++numbanker;
      }
      printf ("bankermode: ");
      printimat (ztypes, 1, numeg);
    }
  }
  if (popfilename != NULL) {
    bbestmode = NO;
    nqlist = numlines (popfilename);
    ZALLOC (qlist, 4, char **);
    for (k = 0; k < 4; ++k) {
      ZALLOC (qlist[k], nqlist, char *);
    }
    nqlist =
      getnamesstripcolon (&qlist, nqlist, 4, popfilename, locount, hicount);
    numeg = 0;
    printf ("number of quadruples %d\n", nqlist);
    fflush (stdout);
    for (k = 0; k < 4; ++k) {
      for (j = 0; j < nqlist; ++j) {
        sx = qlist[k][j];
        t1 = indxstring (eglist, numeg, sx);
        if (t1 >= 0)
          continue;
        eglist[numeg] = strdup (sx);
        ++numeg;
        setstatus (indivmarkers, numindivs, sx);
      }
    }
  }

  ckdups (eglist, numeg);

  ZALLOC (nsamppops, numeg, int);

  for (i = 0; i < numindivs; i++) {
    indx = indivmarkers[i];
    if (indx->ignore)
      continue;
    k = indxindex (eglist, numeg, indx->egroup);
    if (k < 0)
      continue;
    ++nsamppops[k];
  }

  if (popfilename == NULL)
    seteglist (indivmarkers, numindivs, poplistname);   // null if poplistname == NULL

  for (i = 0; i < numeg; i++) {
    t = nsamppops[i];
    if (t == 0)
      fatalx ("No samples: %s\n", eglist[i]);
    printf ("%3d %20s %4d\n", i, eglist[i], t);
  }

  printf ("jackknife block size: %9.3f\n", blgsize);

  outpop = strdup ("NONE");
  outnum = 999;

  for (i = 0; i < numsnps; i++) {
    cupt = snpmarkers[i];
    chrom = cupt->chrom;
    if (cupt->chrom == (numchrom + 1))
      cupt->ignore = YES;
    if ((xchrom > 0) && (chrom != xchrom))
      cupt->ignore = YES;
  }

  ZALLOC (xindex, numindivs, int);
  ZALLOC (xindlist, numindivs, Indiv *);
  nrows = loadindx (xindlist, xindex, indivmarkers, numindivs);
  ZALLOC (xtypes, nrows, int);
  for (i = 0; i < nrows; i++) {
    indx = xindlist[i];
    k = indxindex (eglist, numeg, indx->egroup);
    xtypes[i] = k;
    t = strcmp (indx->egroup, outpop);
    if (t == 0)
      xtypes[i] = outnum;
  }


  for (i = 0; i < numsnps; i++) {
    cupt = snpmarkers[i];
    cupt->weight = 1.0;
  }
  numsnps = rmsnps (snpmarkers, numsnps, NULL); //  rid ignorable snps
  if (numsnps == 0)
    fatalx ("no valid snps\n");

  setmgpos (snpmarkers, numsnps, &maxgendis);
  if ((maxgendis < .00001) || (gfromp == YES))
    setgfromp (snpmarkers, numsnps);

  nblocks = numblocks (snpmarkers, numsnps, blgsize);
  ZALLOC (blstart, nblocks, int);
  ZALLOC (blsize, nblocks, int);
  ZALLOC (tagnums, numsnps, int);

  if (popsizelimit > 0) {
    setplimit (indivmarkers, numindivs, eglist, numeg, popsizelimit);
  }

  // ncols = loadsnpx (xsnplist, snpmarkers, numsnps, indivmarkers);
  int ncols = numsnps ;

  printf ("snps: %d  indivs: %d\n", ncols, nrows);
  setblocks (blstart, blsize, &xnblocks, snpmarkers, ncols, blgsize);
// loads tagnumber
  printf ("number of blocks for jackknife: %d\n", xnblocks);
  nblocks = xnblocks;

  printf ("nrows, ncols: %d %d\n", nrows, ncols);
  int nh2 = numeg * (numeg - 1) / 2;

  ZALLOC (f2ind, numeg * numeg, int);
  ZALLOC (ind2f, nh2, int);

  for (int k = 0, a = 0; a < numeg; ++a) {
    for (int b = a + 1; b < numeg; ++b, ++k) {
      ind2f[k] = a * numeg + b;
      f2ind[a * numeg + b] = k;
      f2ind[b * numeg + a] = k;
    }
  }

  double *abx, *bax, *f2;
  ZALLOC (abx, nh2, double);
  ZALLOC (bax, nh2, double);
  if (dotree) ZALLOC (f2,  nh2, double);

  const int nstats = popfilename
    ? nqlist
    : numeg*(numeg-1)*(numeg-2)*(numeg-3) / 8 ;

  struct dsc_state *dsc_states = malloc( nstats * sizeof( struct dsc_state ) ) ;
  struct gettreelen_state *tree_states = malloc( nstats * sizeof( struct gettreelen_state ) ) ;

  for( struct dsc_state *s = dsc_states ; s != dsc_states+nstats ; ++s ) getdsc_pre(s, nblocks);
  for( struct gettreelen_state *st = tree_states ; st != tree_states+nstats ; ++st ) gettreelen_pre( st, nblocks ) ;

  // Oh god, this is sooo inefficient...
  int **counts = initarray_2Dint (numeg, 2, 0);
  for (int k = 0; k < ncols; ++k, next_genostream( &genos ) ) {
    if( !valid_genostream( &genos ) ) {
      fprintf( stderr, "broken genotype stream after %d SNPs\n", k );
      exit(1);
    }

    if (numvalidgtx (indivmarkers, snpmarkers[k], &genos, YES, numindivs) <= 1) {
      printf ("nodata: %20s\n", snpmarkers[k]->ID);
      cupt->ignore = YES;
      continue;
    }

    for (i = 0; i < numeg; i++) ivclear (counts[i], 0, 2);
    if (xmode && (xchrom == (numchrom + 1))) {
      countpopsx1(counts, /* xsnplist */ snpmarkers[k], &genos, xindlist, xindex, xtypes, nrows);
    } else {
      countpops1(counts, /* xsnplist */ snpmarkers[k], &genos, xindex, xtypes, nrows);
    }

    if (verbose && k<100) {
      printf ("zzcount: %3d ", k);
      for (j = 0; j < numeg; ++j) {
        printf ("%3d %3d  ", counts[j][0], counts[j][1]);
      }
      printnl ();
    }

    int bcols = /* xsnplist */ snpmarkers[k]->tagnumber;
    setabx (abx, bax, counts, numeg);
    if(dotree) {
      vclear (f2, -10.0, nh2);
      setf2 (f2, counts, numeg);
    }

    if( !popfilename ) {
      struct dsc_state *s = dsc_states ;  
      struct gettreelen_state *st = tree_states ;

      for (int a = 0; a < numeg; a++) {
        for (int b = a + 1; b < numeg; b++) {
          for (int c = b + 1; c < numeg; c++) {
            for (int d = c + 1; d < numeg; d++, s+=3) {

              xind[0] = a;
              xind[1] = b;
              xind[2] = c;
              xind[3] = d;

              if( islegal (xind, ztypes, bankermode, numeg) ) {
                getdsc_step(s+0, abx, bax, a, b, c, d, numeg, bcols) ;
                getdsc_step(s+1, abx, bax, a, c, b, d, numeg, bcols) ;
                getdsc_step(s+2, abx, bax, a, d, b, c, numeg, bcols) ;
              }
              // gettreelen (tlenz, tlen, f2, abx, bax, ncols, rpat[k], numeg, bcols, nblocks);
              if(dotree) {
                xcopy (rpat[0], a, b, c, d);
                xcopy (rpat[1], a, c, b, d);
                xcopy (rpat[2], a, d, b, c);

                for (int kk = 0; kk < 3; ++kk)
                  gettreelen_step (st, f2, abx, bax, rpat[kk], numeg, bcols) ;
              }
            }
          }
        }
      }
    } else {
      for (tt = 0; tt < nqlist; ++tt) {
        int a = indxindex (eglist, numeg, qlist[0][tt]);
        int b = indxindex (eglist, numeg, qlist[1][tt]);
        int c = indxindex (eglist, numeg, qlist[2][tt]);
        int d = indxindex (eglist, numeg, qlist[3][tt]);

        if ((a == b) || (a == c) || (a == d) || (b == c) || (b == d) || (c == d)) {
          printbadql (qlist, tt, eglist, numeg);
          continue;
        }

        // getdsc (&rscore[0], &dstat[0], abx, bax, ncols,
        //         a, b, c, d, numeg, bcols, nblocks, ssize[0]);
        getdsc_step(&dsc_states[tt], abx, bax, a, b, c, d, numeg, bcols) ;
        if (dotree) {
          xcopy (rpat[0], a, b, c, d);
          gettreelen_step (&tree_states[tt], f2, abx, bax, rpat[0], numeg, bcols) ;
        }
      }
    }
  }
  close_genostream( &genos ) ;

  if( !popfilename ) {
    struct dsc_state *s = dsc_states ;  
    struct gettreelen_state *st = tree_states ;

    for (int a = 0; a < numeg; a++) {
      for (int b = a + 1; b < numeg; b++) {
        for (int c = b + 1; c < numeg; c++) {
          for (int d = c + 1; d < numeg; d++, s+=3) {
            getdsc_post(s+0, &rscore[0], &dstat[0], a, b, c, d, nblocks, ssize[0]);
            getdsc_post(s+1, &rscore[1], &dstat[1], a, c, b, d, nblocks, ssize[1]);
            getdsc_post(s+2, &rscore[2], &dstat[2], a, d, b, c, nblocks, ssize[2]);
            vclip (rscore, rscore, -100.0, 100.0, 3);

            for (int kk = 0; kk < 3; ++kk) {
              serr[kk] = 1.0;
              if (rscore[kk] != 0)
                serr[kk] = dstat[kk] / rscore[kk];
            }

            xcopy (rpat[0], a, b, c, d);
            xcopy (rpat[1], a, c, b, d);
            xcopy (rpat[2], a, d, b, c);

            ivzero (bbest, 3);

            if (bbestmode) {
              vabs (absscore, rscore, 3);
              vlmaxmin (absscore, 3, NULL, &k);
              bbest[k] = 1;
            }

            for (k = 0; k < 3; ++k) {
              copyiarr (rpat[k], popx, 4);
              printf ("result: ");
              sz = ssize[k];

              if (bankermode) {
                for (i = 0; i < 4; i++) {
                  t = popx[i];
                  printf ("%1d", ztypes[t]);
                }
                printf ("  ");
              }
              for (i = 0; i < 4; i++) {
                t = popx[i];
                printf ("%10s ", eglist[t]);
              }

              if (f4mode == NO)
                printf (" %10.4f", dstat[k]);
              if (f4mode == YES)
                printf (" %12.6f", dstat[k]);

              if (printsd) {
                printf (" %12.6f", serr[k]);
              }

              printf (" %9.3f", rscore[k]);
              if (dotree) {
                // gettreelen (tlenz, tlen, f2, abx, bax, ncols, rpat[k], numeg, bcols, nblocks);
                gettreelen_post (st++, tlenz, tlen, nblocks) ;

                ivzero (lenz, 5);
                for (i = 0; i < 5; i++) {
                  if (tlenz[i] > 0)
                    continue;
                  lenz[i] = nnint (-10.0 * tlenz[i]);
                  lenz[i] = MIN (lenz[i], 9);
                }
                for (i = 0; i < 5; i++) {
                  printf ("% 1d", lenz[i]);
                }
              }
              if (bbest[k] == 1) {
                printf ("%6s", " best ");
              }
              else {
                printf ("%6s", " - ");
              }
              if (printssize) {
                printf ("%6d ", sz[0]);
                printf ("%6d ", sz[1]);
                printf ("%6d ", sz[2]);
              }
              printnl ();
            }
          }
        }
      }
    }
  } else {
    for (tt = 0; tt < nqlist; ++tt) {
      int a = indxindex (eglist, numeg, qlist[0][tt]);
      int b = indxindex (eglist, numeg, qlist[1][tt]);
      int c = indxindex (eglist, numeg, qlist[2][tt]);
      int d = indxindex (eglist, numeg, qlist[3][tt]);

      if ((a == b) || (a == c) || (a == d) || (b == c) || (b == d) || (c == d))
        continue;

      getdsc_post(&dsc_states[tt], &rscore[0], &dstat[0], a, b, c, d, nblocks, ssize[0]);
      serr[0] = 1.0;
      if (rscore[0] != 0.0)
        serr[0] = dstat[0] / rscore[0];

      xcopy (rpat[0], a, b, c, d);
      vclip (rscore, rscore, -100.0, 100.0, 3);

      k = 0;
      sz = ssize[k];

      copyiarr (rpat[k], popx, 4);
      printf ("result: ");

      for (i = 0; i < 4; i++) {
        t = popx[i];
        printf ("%10s ", eglist[t]);
      }

      if (f4mode == NO)
        printf (" %10.4f", dstat[k]);
      if (f4mode == YES)
        printf (" %12.6f", dstat[k]);

      if (printsd) {
        printf (" %12.6f", serr[k]);
      }
      printf (" %9.3f", rscore[k]);

      if (dotree) {
        gettreelen_post (&tree_states[tt], tlenz, tlen, nblocks) ;
        ivzero (lenz, 5);
        for (i = 0; i < 5; i++) {
          if (tlenz[i] > 0)
            continue;
          lenz[i] = nnint (-10.0 * tlenz[i]);
          lenz[i] = MIN (lenz[i], 9);
        }
        for (i = 0; i < 5; i++) {
          printf ("% 1d", lenz[i]);
        }
      }

      if (printssize) {
        printf ("%6d ", sz[0]);
        printf ("%6d ", sz[1]);
        printf ("%6d ", sz[2]);
      }
      printnl ();

    }
  }
  free( dsc_states ) ;
  free( tree_states ) ;

  printf ("## end of run\n");
  return 0;
}

void
printbadql (char ***qlist, int tt, char **eglist, int numeg)
{
  int a, b, c, d;

  a = indxindex (eglist, numeg, qlist[0][tt]);
  b = indxindex (eglist, numeg, qlist[1][tt]);
  c = indxindex (eglist, numeg, qlist[2][tt]);
  d = indxindex (eglist, numeg, qlist[3][tt]);

  printf ("***warning: repeated population: ");
  printf ("%s ", eglist[a]);
  printf ("%s :  ", eglist[b]);
  printf ("%s ", eglist[c]);
  printf ("%s ", eglist[d]);
  printnl ();
}

void
readcommands (int argc, char **argv)
{
  int i, haploid = 0;
  phandle *ph;
  char str[5000];
  char *tempname;
  int n;

  while ((i = getopt (argc, argv, "l:h:p:vV")) != -1) {

    switch (i) {

    case 'l':
      locount = atoi (optarg);
      break;

    case 'h':
      hicount = atoi (optarg);
      break;

    case 'p':
      parname = strdup (optarg);
      break;

    case 'v':
      printf ("version: %s\n", WVERSION);
      break;

    case 'V':
      verbose = YES;
      break;

    case '?':
      printf ("Usage: bad params.... \n");
      fatalx ("bad params\n");
    }
  }


  if (parname == NULL) {
    fprintf (stderr, "no parameters\n");
    return;
  }

  pcheck (parname, 'p');
  printf ("%s: parameter file: %s\n", argv[0], parname);
  ph = openpars (parname);
  dostrsub (ph);

  getstring (ph, "genotypename:", &genotypename);
  getstring (ph, "snpname:", &snpname);
  getstring (ph, "indivname:", &indivname);
  getstring (ph, "poplistname:", &poplistname);
  getstring (ph, "popfilename:", &popfilename);
  //  getstring(ph, "outpop:", &outpop) ;
  getstring (ph, "output:", &outputname);
  getstring (ph, "outputname:", &outputname);
  getstring (ph, "badsnpname:", &badsnpname);
  getstring (ph, "msjackname:", &msjackname);
  getstring (ph, "id2pops:", &id2pops);
  getint (ph, "msjackweight:", &msjackweight);
  getdbl (ph, "blgsize:", &blgsize);
  getint (ph, "dotree:", &dotree);
  getint (ph, "printsd:", &printsd);
  getint (ph, "nochrom:", &zchrom);
  getint (ph, "locount:", &locount);
  getint (ph, "hicount:", &hicount);
  getint (ph, "numchrom:", &numchrom);

  getint (ph, "noxdata:", &noxdata);
  getint (ph, "colcalc:", &colcalc);
  getint (ph, "inbreed:", &inbreed);
  getint (ph, "printssize:", &printssize);

  getint (ph, "nostatslim:", &nostatslim);
  getint (ph, "popsizelimit:", &popsizelimit);
  getint (ph, "gfromp:", &gfromp);      // gen dis from phys
  getint (ph, "fullmode:", &fullmode);
  getint (ph, "bankermode:", &bankermode);
  getint (ph, "forceclade:", &forceclade);      // in bankermode one clade must be all banker = 2
  getint (ph, "bbestmode:", &bbestmode);        // print "best" for triples
// getint(ph, "fancynorm:", &fancynorm) ; 
// getint(ph, "usenorm:", &fancynorm) ;  /* changed 11/02/06 */
  getdbl (ph, "jackquart:", &jackquart);
  getint (ph, "jackweight:", &jackweight);
  getint (ph, "chrom:", &xchrom);
  getint (ph, "xmode:", &xmode);
  getint (ph, "f4mode:", &f4mode);


  printf ("### THE INPUT PARAMETERS\n");
  printf ("##PARAMETER NAME: VALUE\n");
  writepars (ph);

}

double
getrs (double *f4, double *f4sig, double *f2, double *f2sig,
       int a, int b, int c, int d, int numeg, double *rho)
{

  double y1, y2, ya, yb;

  y1 = dump4 (f4, a, b, c, d, numeg);
  y2 = dump4 (f4sig, a, b, c, d, numeg);
  ya = f2[a * numeg + b];
  yb = f2[c * numeg + d];

  *rho = y1 / sqrt (ya * yb);

  return y1 / y2;

}

int
islegal (int *xind, int *types, int mode, int n)
{
// at least 1 type 1 
// check all bankers set.  OR at least 2 bankers 
  int cc[3];
  int zz[MAXPOPS];
  int i, x, k, t;


  if (mode == NO)
    return YES;
  ivzero (zz, n);
  ivzero (cc, 3);
  for (i = 0; i < 4; i++) {
    t = xind[i];
    x = types[t];
    ++cc[x];
    ++zz[t];
  }

/**
  printf("zzq1 ") ;  printimat(xind, 1, 4) ;
  printimat(cc, 1, 4) ;
  printimat(zz, 1, n) ;
  printimat(types, 1, n) ;
*/

  if (cc[1] == 0)
    return NO;
  if (cc[2] >= 2)
    return YES;
//  if (cc[1] == 2) return NO ; 
  for (t = 0; t < n; ++t) {
    if (types[t] != 2)
      continue;
    if (zz[t] == 0)
      return NO;
  }
  return YES;
}

int
isclade (int *rr, int *zz)
// is a clade banker only? 
{

  int a, b;

  a = rr[0];
  b = rr[1];
  if ((zz[a] == 2) && (zz[b] == 2))
    return YES;

  a = rr[2];
  b = rr[3];
  if ((zz[a] == 2) && (zz[b] == 2))
    return YES;

  return NO;

}

void
regesttree (double *ans, double *xn, double xd)
{
  int a, b, c, k;
  double *co, *rr;
  double f;

  ZALLOC (co, 6 * 5, double);
  ZALLOC (rr, 6, double);

  k = 0;
  a = 0;
  b = 1;
  f = xn[k] / xd;
  co[k * 5 + 0] = co[k * 5 + 1] = 1;
  rr[k] = f;

  k = 1;
  a = 0;
  b = 2;
  f = xn[k] / xd;
  co[k * 5 + 0] = co[k * 5 + 2] = co[k * 5 + 3] = 1;
  rr[k] = f;

  k = 2;
  a = 0;
  b = 3;
  f = xn[k] / xd;
  co[k * 5 + 0] = co[k * 5 + 2] = co[k * 5 + 4] = 1;
  rr[k] = f;

  k = 3;
  a = 1;
  b = 2;
  f = xn[k] / xd;
  co[k * 5 + 1] = co[k * 5 + 2] = co[k * 5 + 3] = 1;
  rr[k] = f;

  k = 4;
  a = 1;
  b = 3;
  f = xn[k] / xd;
  co[k * 5 + 1] = co[k * 5 + 2] = co[k * 5 + 4] = 1;
  rr[k] = f;

  k = 5;
  a = 2;
  b = 3;
  f = xn[k] / xd;
  co[k * 5 + 3] = co[k * 5 + 4] = 1;
  rr[k] = f;

  regressit (ans, co, rr, 6, 5);

  free (co);
  free (rr);

}

void
ckdups (char **list, int n)
{
  int t, i, j;

  for (i = 0; i < n; ++i) {
    for (j = i + 1; j < n; ++j) {
      t = strcmp (list[i], list[j]);
      if (t == 0)
        fatalx ("dup in list: %s\n", list[i]);
    }
  }
}
