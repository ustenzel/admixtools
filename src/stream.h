#ifndef INCLUDED_STREAM_H
#define INCLUDED_STREAM_H

#include <zlib.h>

/* Streaming access to genotype information.  Right now, only Eigenstrat
 * format is supported, and only qpDstat uses it.  Code is cloned and
 * liberated from ineigenstrat(), because patching that thing is just
 * not feasible. */

/* Strategy:  We read chunks from a gzfile into a buffer.  That way, we
 * get sane(!) gzip decompression for free. */

struct geno_stream {
    char *current ;
    char *end ;
    char *buffer ;
    char *bufend ;
    gzFile fd ;
} ;

/* Open an Eigenstrat file as stream.  Arguments are an uninitialized
 * geno_stream structure and the file name.  (Not supplied are number of
 * SNPs, number of inDUHviduals, or any meta information.  We can't use
 * it anyway.)  The return value is 0 if everything went well, otherwise
 * -1.  If something went wrong, errno contains the last error.  The
 *  stream is intially positioned so the first SNP record can be
 *  accessed. */
int open_genostream( struct geno_stream *stream, const char *gname ) ;

/* Closes an Eigenstrat stream. */
int close_genostream( struct geno_stream *stream ) ;

/* Moves the stream to the next record. */
void next_genostream( struct geno_stream *stream ) ;

/* Tests if the stream hasn't ended */
int valid_genostream( struct geno_stream *stream ) ;

/* Returns the genotype for the k-th individual in the current record.
 * This function causes undefined behaviour on an invalid file or when
 * called at end-of-file. */
int get_genostream( struct geno_stream *stream, int k ) ;

#endif
