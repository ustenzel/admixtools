#include "stream.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

static void fill_buf( struct geno_stream *stream )
{
    // nothing to do if we already closed the file
    while( stream->fd ) {

        // check if a full line is in the buffer:
        for( char *p = stream->current ; p != stream->end ; ++p )
            if( *p == '\n' ) return ;

        // no full line.  move stuff around, fill up
        size_t len = stream->end - stream->current ;
        memmove( stream->buffer, stream->current, len ) ;
        stream->current = stream->buffer ;
        int rem = stream->bufend - stream->buffer - len ;
        int len2 = gzread( stream->fd, stream->buffer + len, rem ) ;
        stream->end = stream->buffer + len + ( len2 > 0 ? len2 : 0 ) ;

        if( len2 < rem ) {
            // hit EOF or errored
            gzclose( stream->fd ) ;
            stream->fd = 0 ;
        }
    }
}

static void skip_line( struct geno_stream *stream ) 
{
    while( stream->current != stream->end
            && *stream->current != '\n' )
        ++stream->current ;
    if( stream->current != stream->end )
        ++stream->current ;
    fill_buf( stream ) ;
}

static void skip_comments( struct geno_stream *stream ) 
{
    while( stream->current != stream->end
            && *stream->current == '#' )
        skip_line( stream ) ;
}


int open_genostream( struct geno_stream *stream, const char *gname ) 
{
    stream->fd = gzopen( gname, "rb" ) ;
    if( stream->fd ) {
        stream->buffer = malloc( 128*1024 ) ;
        if( stream->buffer ) {
            stream->bufend = stream->buffer + 128*1024 ;
            stream->current = stream->end = stream->buffer ;
            fill_buf( stream ) ;
            skip_comments( stream ) ;
            return 0 ;
        }
        gzclose( stream->fd ) ;
    }
    return -1 ;
}

/* Closes an Eigenstrat stream. */
int close_genostream( struct geno_stream *stream )
{
    free( stream->buffer ) ;
    if( stream->fd )
        return gzclose( stream->fd ) ;
    else
        return 0 ;
}

/* Skip the current line.  Don't run over the end. */
void next_genostream( struct geno_stream *stream )
{
    skip_line( stream ) ;
    skip_comments( stream ) ;
}

int valid_genostream( struct geno_stream *stream )
{
    return stream->current != stream->end ;
}

/* Returns the genotype for the k-th individual in the current record.
 * This function causes undefined behaviour on an invalid file or when
 * called at end-of-file. */
int get_genostream( struct geno_stream *stream, int k ) 
{
    if( *stream->current == 'X' ) return -1 ;
    if( stream->current + k < stream->end ) {
        switch( stream->current[k] ) {
            case '0': return 0 ;
            case '1': return 1 ;
            case '2': return 2 ;
            case '9': return -1 ;
        }
    }
    fprintf( stderr, "Invalid genotype (%x), at offset %ld, individual %d.\n",
            stream->current[k], stream->current - stream->buffer, k ) ;
    exit(1) ;
}


